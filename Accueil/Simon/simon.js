var facile = document.getElementsByClassName("facile");
var moyen = document.getElementsByClassName("moyen");
var difficile = document.getElementsByClassName("difficile");

var nb = 4;

//definition des boutons
document.getElementById("startf").addEventListener("click", function () {
	document.getElementById("zone").classList.remove("large");
	for (var i = 0; i < moyen.length; i++) {
		moyen[i].classList.add("hide");
	}
	for (var i = 0; i < difficile.length; i++) {
		difficile[i].classList.add("hide");
	}
	nb = 4;
});

document.getElementById("startm").addEventListener("click", function () {
	document.getElementById("zone").classList.add("large");
	for (var i = 0; i < moyen.length; i++) {
		moyen[i].classList.remove("hide");
	}
	for (var i = 0; i < difficile.length; i++) {
		difficile[i].classList.add("hide");
	}
	nb = 6;
});

document.getElementById("startd").addEventListener("click", function () {
	document.getElementById("zone").classList.add("large");
	for (var i = 0; i < moyen.length; i++) {
		moyen[i].classList.remove("hide");
	}
	for (var i = 0; i < difficile.length; i++) {
		difficile[i].classList.remove("hide");
	}
	nb = 9;
});

var btn = [document.getElementById("a"),document.getElementById("b"),document.getElementById("c"),document.getElementById("d"),document.getElementById("e"),document.getElementById("f"),document.getElementById("g"),document.getElementById("h"),document.getElementById("i")];
var cpuseq = [];
var round = 0;

function sound1(){
       var audio = document.getElementById("startaudio");
       audio.play();
}

function sound2(){
       var audio = document.getElementById("resetaudio");
       audio.play();
}

function sound3(){
       var audio = document.getElementById("goaudio");
       audio.play();
}
//0 : initialisation
sound1();

for (var i = 0; i < 4; i++) {
	btn[i].classList.add("actif");
	setTimeout(function(action) {
          action.classList.remove("actif");
      	}, 200*(i+1), btn[i]);
}

var reload = function () {
	window.location.reload(true);
}

document.getElementById("reset").addEventListener("click", reload);
document.getElementById("rd").innerHTML = "Round "+round+"";
document.getElementById("start").addEventListener("click", sound2);
document.getElementById("start").addEventListener("click", game);

document.getElementById("result").innerHTML = "Choisis ton niveau de difficulté";

function game() {

	round = 0;
	cpuseq = [];
	
	for (var i = 0; i < 4; i++) {
	cpuseq.push(btn[Math.floor(Math.random()*nb)]);
}

//1 : montrer au joueur les boutons choisis
function newGame()	{
	round++;
	document.getElementById("rd").innerHTML = "Round "+round+"";
	document.getElementById("result").innerHTML = "Mémorise la séquence !";
	var id = 0;
	
	// allumer le bouton
	var ct = function() {
		cpuseq[id].classList.add("actif");

	// différer l'extinction de 600ms
		setTimeout(function(action) {
	            action.classList.remove("actif");
	        }, 600/(round/3), cpuseq[id]);

	// différer l'allumage du bouton suivant dans 800ms     
		id++;
		
		if (id < cpuseq.length) {	 
			setTimeout(ct, 800/(round/3));
		}	else {

	// quand la séquence est finie, on laisse le joueur jouer
				player();
			}
	}
	ct();
}

//2 : saisie du joueur et comparaison
function player()	{
	document.getElementById("result").innerHTML = "A ton tour !";
	var id = 0;

	// au bout de cinq secondes, gameover
	var time = setTimeout(gameOver, 5000);

	// écoutons les clics
	for (var i = 0; i < btn.length; i++) {
		btn[i].addEventListener("click", check);
	}

	function gameOver() {

		document.getElementById("result").innerHTML = "Temps écoulé, recommence !";

	// on retire les listeners
		for (var i = 0; i < btn.length; i++) {
			btn[i].removeEventListener("click", check);
		}

		for (var i = 0; i < btn.length; i++) {
			btn[i].classList.add("actif");
			setTimeout(function(action) {
		          action.classList.remove("actif");
		      	}, 200, btn[i]);
		} sound3();
	}

	function check() {
	// dans tous les cas, on retire le timeout qui lance le gameover à 5s
		clearTimeout(time);

	// this désigne le bouton cliqué
		if (this != cpuseq[id]) {

	// perdu
			cpuseq = [];
			id = 0
			document.getElementById("result").innerHTML = "Perdu !";
			for (var i = 0; i < 4; i++) {
				btn[i].classList.add("actif2");
					setTimeout(function(action) {
          action.classList.remove("actif2");
      	}, 1000, btn[i]);
}
			sound3();

	// on retire les listeners
			for (var i = 0; i < btn.length; i++) {
				btn[i].removeEventListener("click", check);
			}
		}	else {

	// bon bouton
				id++;
				if (id < cpuseq.length) {

	// s'il reste des boutons à cliquer, c'est tout ce qu'il y a à faire, les boutons continuent d'écouter, le tour du joueur continue
					time = setTimeout(gameOver, 5000);
				}	else {

	// round gagné, on ajoute un bouton à la séquence
						cpuseq.push(btn[Math.floor(Math.random()*4)]);
						id = 0;
						setTimeout(newGame, 300);

	// on retire les listeners, l'ordi montre la séquence
						for (var i = 0; i < btn.length; i++) {
							btn[i].removeEventListener("click", check);
						}
				}
		}
	}
}	newGame();
}