<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="simon.css">
		<link rel="stylesheet" type="text/css" href="../style.css">
		<title>Simon</title>		
	</head>

	<body>
		<?php include('../menu.php'); ?>
		<main class='simon'>
			<h1>Simon Game</h1>
				<p class="menu">
					<button id="start" class="in">Start</button>
					<button id="startf" class="go">Facile</button>
					<button id="startm" class="go">Moyen</button>
					<button id="startd" class="go">Difficile</button>
					<button id="reset" class="in">Reset</button>
					<audio id="startaudio" src="start.mp3" ></audio>
					<audio id="goaudio" src="gameover.mp3" ></audio>
					<audio id="resetaudio" src="reset.mp3" ></audio>
				</p>

				<p id="rd">
				</p>

			<section id="zone">
				<button id="a" class="facile"></button>
				<button id="c" class="facile"></button>
				<button id="b" class="facile"></button>
				<button id="d" class="facile"></button>
				<button id="e" class="moyen hide"></button>
				<button id="f" class="moyen hide"></button>
				<button id="g" class="difficile hide"></button>	
				<button id="h" class="difficile hide"></button>
				<button id="i" class="difficile hide"></button>
			</section>

			<p id="result">
				</p>
		</main>
		<script src="simon.js" type="text/javascript"></script>
	</body>
</html>