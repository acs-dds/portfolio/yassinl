<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerRegister extends CI_Controller {	

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('date');	
		// $this->load->library('session');		
		// $this->output->enable_profiler(true);
		$this->load->model('ModelRegister');
	}

	//page inscription
	public function register(){

		if($this->input->post() == false){//je mets le formulaire a false car il a true par default.

		} else if(!empty($user = htmlentities($this->input->post('user')))&& !empty($email = htmlentities($this->input->post('email'))) && !empty($pass = $this->input->post('pass'))){//j'evite d'envoyer des message vide et protège mes input.

			$this->ModelRegister->reg($user,$email, $pass);	/*
			Après inscription je redirige ver le login
			*/
			redirect('http://yassinl.dijon.codeur.online/ci_tchat/index.php/connecter');
		} else{
			//si les champs ne sont pas renseigné je return un message d'erreur.
			var_dump(false, 'ERROR !!! ');
		}

		$this->load->view('read/header');
		$this->load->view('register');//main
		$this->load->view('read/footer');

		//j'affiche la liste_users temporairement pour le traitement des données

		$data['req'] = $this->ModelRegister->read();
		$this->load->view('liste_users', $data);//main			
	}	

	//Page liste des utilisateur inscrit *** Je desactive la fonction temporairement***
	// public function listeUser() {

	// 	$data['req'] = $this->ModelRegister->read();

	// 	$this->load->view('read/header');
	// 	$this->load->view('liste_users', $data);//main
	// 	$this->load->view('read/footer');		
	// }

}