<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelMessage extends CI_Model{

	public function __construct(){
		parent::__construct();

		$this->load->database();
	}

	/*
	je créer ma méthode qui me permettra avec 3 arguments
	$message permet d'inserer un message,
	$channel permet de selectionner un channel,
	$id_client prepare la session de l user qui ecrira se message.
	*/

	public function message($message, $channel, $id_client) {
		$data = array(
			"message"   => $message,		
			"id_salon"   => $channel,
			"id_client"   =>  $id_client		
			);
		return $this->db->insert("message", $data);
	}

	/*
	je créer une methode qui recupère l'id de la ligne client_user de ma table
	*/

	public function getUserId($user_name){
		$this->db->select('id');
		$this->db->from('liste_users');
		$this->db->where('client_user', $user_name);

		return $this->db->get()->row("id");
	}

	//je créer une methode qui recuperera tout les utilisateur inscrit ainsi je pourrai les traité et faire en sorte qu'on affichge que les client_online dans le futur.

	// public function getUserOnline(){
	// 	$this->db->select('client_user');
	// 	$this->db->from('liste_users');

	// 	return $this->db->get()->result_array();
	// }

	
	/*
		je créer une méthode qui lira chaque conversation complète de chaque utilisateur qui postera un message.
	*/
	public function readMessage($channel){		
		/*LES JOINTURES REVOIR LES COURS PAS ENCORE ALAISE AVEC*/
		$select = "SELECT liste_users.client_user,message.message,message.temps_message,salon.libelle

				  FROM message
				  
				  JOIN salon ON message.id_salon = salon.id
				
				  JOIN liste_users ON message.id_client = liste_users.id
				
				  WHERE salon.libelle = ? ORDER BY temps_message DESC";

		$query = $this->db->query($select, array($channel));
		return $query->result_array();
	}
}

