<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelRegister extends CI_Model{

	public function __construct(){
		parent::__construct();

		$this->load->database();
	}

	//Methode Inscription
	/*
	je créer ma methode pour pouvoir me m'inscrire ainsi je pourrai me login dans le futur.
	*/
	public function reg($user, $email, $pass) {
		$data = array(
			"client_user" => $user,
			"email"       => $email,
			"pass" => $this->hash_password($pass)
			);

		return $this->db->insert("liste_users", $data);
	}	
	/*
	je créer ma methode qui recupere la liste_users .
	*/
	public function read(){
		
		$this->db->from("liste_users");
		$this->db->order_by("temps_reel desc");		

		return $this->db->get()->result();
	}	

	private function hash_password($pass){
		return password_hash($pass, PASSWORD_DEFAULT);
	}
}
