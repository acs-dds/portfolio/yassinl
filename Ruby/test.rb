require "rubygems"
require "sinatra"
require "pg"
require "json"

# db_params = {
#   host: "vps338664.ovh.net",
#   dbname: "esperance",
#   user: "eyz",
#   password: "navi"
# }
# psql = PG::Connection.new(db_params)
conn = PG::Connection.open(
	:host => "vps338664.ovh.net",
	:dbname => "esperance",
	:user => "eyz",
	:password => "navi"
)

get "/" do
	'HOME <a href="/page1">click</a>'
end

get "/page1" do
	'Version of libpg: ' + PG.library_version.to_s
	# esperance = psql.exec("SELECT * FROM departements")
	# esperance[0].inspect

	res = conn.exec("SELECT * FROM departements")
	res.each do |row|
		puts JSON.dump(row.to_json).to_s
	end
end
