<?php
require_once __DIR__.'/../models/HotelModel.php';
Class HotelController 
{
	private $model;

	public function __construct()
	{

		$this->model = new HotelModel();
	}

	public function getRegister() 
	{

		if(isset($_POST['pseudo']) && isset($_POST['mail']) && isset($_POST['pass'])) {
			$client = $_POST['pseudo'];
			$mail = $_POST['mail'];
			$pass = $_POST['pass'];

			$this->model->register($client, $mail, $pass);
		} else {
			die('error');
		}
	}

	public function newHotel() 
	{
		if (isset($_POST['name']) && isset($_POST['city']) && isset($_POST['category'])) {

			$name = $_POST['name'];
			$city = $_POST['city'];
			$category = $_POST['category'];

			$this->model->addHotel($name, $city, $category);

			header('Location: /hotel_management');
			exit;
		}
	}

	public function getListeHotel()
	{
		return $this->model->listeHotel();		
	}

	public function getRow()
	{
		if (isset($_GET['id'])) {
			$id = $_GET['id'];

			return $this->model->row($id);
		} else {
			die('error');
		}					
	}

	public function updateRow()
	{
		isset($_GET['id']) ? $id = $_GET['id'] : null;
		if (isset($_POST['name']) && isset($_POST['city']) && isset($_POST['category']) && !empty($_POST['name']) && !empty($_POST['city']) && !empty($_POST['category'])) {
			$name = $_POST['name'];
			$city = htmlentities($_POST['city']);
			$category = $_POST['category'];

			$this->model->update($id, $name, $city, $category);

			header('Location: /hotel_management');
			exit;
		}	
	}
	/*
	public function updateRow()
	{
		isset($_GET['id']) ? $id = $_GET['id'] : null;
		if (isset($_POST['name']) && isset($_POST['city']) && isset($_POST['category'])) {
			$name = $_POST['name'];
			$city = $_POST['city'];
			$category = $_POST['category'];

			$this->model->update($id, $name, $city, $category);

			header('Location: /hotel_management');
			exit;
		}
	}
	*/
	public function executeDelete()
	{
		if (isset($_GET['id'])) {
			$id = $_GET['id'];

			$this->model->delete($id);

			header('Location: /hotel_management');
			exit;
		} else {
			die('error');
		}			
	}
}
