<?php 
Class HotelModel 
{
	private $bdd;

	public function __construct() 
	{
		$this->bdd = new PDO("pgsql:host=vps338664.ovh.net;dbname=hotel_management;user=yassinel;password=s5Sx3Uc5");
	}

	public function register($client, $email, $pass)
	{
		$sql = 'INSERT INTO utilisateurs(client, email, pass) VALUES(?, ?, ?)';
		$query = $this->bdd->prepare($sql);
		return $query->execute(array($name, $email, md5($pass)));
	}

	public function addHotel($name, $city, $category)
	{
		$sql = 'INSERT INTO hotel(buisness, city, category) VALUES(?, ?, ?)';
		$requete = $this->bdd->prepare($sql);
		return $requete->execute(array($name, $city, $category));
	}

	public function listeHotel()
	{
		$sql = 'SELECT id, buisness, city, category FROM hotel';
		$requete = $this->bdd->prepare($sql);
		$requete->execute();
		return $requete->fetchAll();
	}	

	public function row($id)
	{	
		$sql = 'SELECT * FROM hotel WHERE id = ?';
		$requete = $this->bdd->prepare($sql);
		$requete->execute(array($id));
		return $requete->fetchAll();
	}

	public function update($id, $name, $city, $category)
	{
		$sql = 'UPDATE hotel SET buisness = ?, city = ?, category = ? WHERE id =' .$id;
		$requete = $this->bdd->prepare($sql);
		return $requete->execute(array($name, $city, $category));		
	}

	public function delete($id)
	{
		$sql = 'DELETE FROM hotel WHERE id = ?';
		$requete = $this->bdd->prepare($sql);
		return $requete->execute(array($id));
	}
}