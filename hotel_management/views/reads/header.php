<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Hôtel</title>
		<link rel="stylesheet" type="text/css" href="views/inc/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="views/inc/css/style.css">
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<div class="container">
                <header>
                    <button class="btn btn-danger" id="hamburger">Clik Me</button>
                    <nav class="block-menu">
                        <a href="#">item 1</a>
                        <a href="#">item 2</a>
                        <a href="#">item 3</a>
                        <a href="#">item 4</a>
                    </nav>
                </header>
