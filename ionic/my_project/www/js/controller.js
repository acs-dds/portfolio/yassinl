angular.module("starter").controller("MainCtrl", function($scope, $ionicPopup) {
	
	$scope.todos = [
		{id: 0, title: "+ 1 000 000 000", description: "blabla blabla", done: false},
		{id: 1, title: "- 5 000", description: "blabla blabla", done: false},
		{id: 2, title: "- 456 220", description: "blabla blabla", done: false}
	];

	// $scope.deleteTodoById = function(id) {
	// 	console.log("id of todo to delete: ", id);
	// }

	$scope.deleteTodoById = function(id){
		for(var i = 0; i < $scope.todos.length; i++){

			if($scope.todos[i].id === id){

				$scope.todos.splice(i, 1);

				return;
			}
		}
	}

	$scope.addTodo = function(){
		$scope.newTodo = {id: $scope.todos.length, title: "", description: ""};
		var toDoTemplate = '<input type="text" ng-model="newTodo.title"></br><textarea ng-model="newTodo.description" rows="5" cols="15"></textarea>';
	

		var myPopup = $ionicPopup.show({
			template: toDoTemplate,
			title: "Add task",
			subTitle: "Adding... !",
			scope: $scope,
			buttons: [
				{
					text: "Cancel",
					onTap: function(e) {
						return;
					}
				},
				{
					text: "Add",
					type: "button-positive",
					onTap: function(e) {
						if(!$scope.newTodo.title) {
							e.preventDefault();
						} else {
							$scope.todos.push($scope.newTodo);
							console.log($scope.todos);
						}
					}
				}		
			]
		})
	}

	$scope.updateTodo = function(todo){
		$scope.updatingTodo = todo;
		var toDoTemplate = '<input type="text" ng-model="updatingTodo.title"></br><textarea ng-model="updatingTodo.description" rows="5" cols="15"></textarea>';
	

		var myPopup = $ionicPopup.show({
			template: toDoTemplate,
			title: "Update task",
			subTitle: "Updating... !",
			scope: $scope,
			buttons: [
				{
					text: "Cancel",
					onTap: function(e) {
						return;
					}
				},
				{
					text: "Update",
					type: "button-positive",
					onTap: function(e) {
						if(!$scope.updatingTodo.title) {
							e.preventDefault();
						} else {
							for(var i = 0; i < $scope.todos.length; i++) {
								if($scope.todos[i].id === $scope.updatingTodo.id){
									$scope.todos[i] = $scope.updatingTodo;
									return;
								}
							}
						}
					}
				}		
			]
		})
	}

});