<?php
namespace YL\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class HomeController extends Controller
{
    public function indexAction()
    {
        return $this->render('YLCoreBundle:Core:index.html.twig');
    }
    public function contactAction(){
        return $this->render('YLCoreBundle:Core:contact.html.twig');
    }
    public function formOffAction(Request $request){

        $session = $request->getSession();

        $session->getFlashBag()->add('info', 'La page contact n\'est pas encore disponible');

        $session->getFlashBag()->add('info', 'Merci de revenir plus tard');

        return $this->redirectToRoute('yl_core_contact');
    }
}
	