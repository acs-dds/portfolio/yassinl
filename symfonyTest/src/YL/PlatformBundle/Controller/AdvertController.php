<?php 
namespace YL\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdvertController extends Controller
{
	public function indexAction()
	{
		// ...
		// Notre liste d'annonce en dur
		$listAdverts = array(
		array(
		'title'   => 'id 1',
		'id'      => 1,
		'author'  => 'CrackJ',
		'content' => 'recherche dev back',
		'date'    => new \Datetime()),
		array(
		'title'   => 'id 2',
		'id'      => 2,
		'author'  => 'CrackJ',
		'content' => 'recherche dev front',
		'date'    => new \Datetime()),
		array(
		'title'   => 'id 3',
		'id'      => 3,
		'author'  => 'CrackJ',
		'content' => 'recherche .....',
		'date'    => new \Datetime())
		);

		// Et modifiez le 2nd argument pour injecter notre liste
		return $this->render('YLPlatformBundle:Advert:index.html.twig', array(
		'listAdverts' => $listAdverts
		));
	}
	public function menuAction($limit)
	{
	// On fixe en dur une liste ici, bien entendu par la suite
	// on la récupérera depuis la BDD !
	$listAdverts = array(
	  array('id' => 2, 'title' => 'Flux id 2'), 
	  array('id' => 5, 'title' => 'Flex id 5'),
	  array('id' => 9, 'title' => 'Plop id 9')
	);

	return $this->render('YLPlatformBundle:Advert:menu.html.twig', array(
	  // Tout l'intérêt est ici : le contrôleur passe
	  // les variables nécessaires au template !
	  'listAdverts' => $listAdverts
	));
	}
	public function viewAction($id)
	{
		$data = array(
				'title'   => 'Annonce Temporaire',
				'id'      => $id,
				'author'  => 'CrackJ',
				'content' => 'Temporaire Test Blabla...',
				'date'    => new \Datetime()
			);

			return $this->render('YLPlatformBundle:Advert:view.html.twig', array(
			'data' => $data
		));
	}
	// Ajoutez cette méthode :
	public function fakeSpamAction(Request $request)
	{
		// On récupère le service
		$antispam = $this->get('yl_platform.antispam');

		// Je pars du principe que $text contient le texte d'un message quelconque
		$text = '...';
		if ($antispam->isSpam($text)) {
		throw new \Exception('Votre message a été détecté comme spam !');
		}

		// Ici le message n'est pas un spam
	}
    public function addAction(Request $request)
    {
        $session = $request->getSession();

    //     // Bien sûr, cette méthode devra réellement ajouter l'annonce

    //     // Mais faisons comme si c'était le cas
        $session->getFlashBag()->add('info', 'Annonce bien ajouter');

        // Le « flashBag » est ce qui contient les messages flash dans la session
    //     // Il peut bien sûr contenir plusieurs messages :
        $session->getFlashBag()->add('info', 'Oui oui, elle est bien ajouter !');

    //     // Puis on redirige vers la page de visualisation de cette annonce
        return $this->redirectToRoute('yl_platform_view', array(
				'id' => '$800.000'				
	        ));
    }
	public function editAction($id)
	{
		// ...

		$advert = array(
		  'title'   => 'Modification de l\'annonce',
		  'id'      => $id,
		  'author'  => 'Crackkxx',
		  'content' => 'Test, GG, flutsh, flex, Blabla…',
		  'date'    => new \Datetime()
		);

		return $this->render('YLPlatformBundle:Advert:edit.html.twig', array(
		  'advert' => $advert
		));
	}

	// public function deleteAction($id)
	// {
	// 	// Ici, on récupérera l'annonce correspondant à $id

	// 	// Ici, on gérera la suppression de l'annonce en question

	// 	return $this->render('YLPlatformBundle:Advert:delete.html.twig');
	// }
}