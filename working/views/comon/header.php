<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>working...</title>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
		<script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>		
	</head>
	<body>
		<header id="header-block">		
			<nav class="navbar navbar-inverse">
				<section class="container">
					<section class="navbar-header">
						<a class="navbar-brand  active" href="http://localhost/working/">Home</a>
					</section>

					<section>
						<ul class="nav navbar-nav">
							<li><a href="http://localhost/working/views/content_portfolio">Portefolio<!-- <b class = "caret"></b> --></a></li>
							<li><a href="http://localhost/working/views/content_tchat.php">Tchat</a></li>
							<li><a href="http://localhost/working/views/content_contact.php">Contact</a></li>
						</ul>
					</section>
				</section>	
			</nav>
		</header>