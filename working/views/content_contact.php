<?php include('comon/header.php'); ?>

<section class="container">
	<form class="form" action="" method="POST">
		<section class="form-group">
			<label for="">Votre nom, prenom :</label>
			<input class="form-control" type="text" name="pseudo"/>
		</section>
		<section class="form-group">
			<label for="">Votre email :</label>
			<input class="form-control" type="email" name="vmail"/>
		</section>
		<section class="form-group">
			<label for="">Votre message :</label>
			<textarea class="form-control" name="mess"></textarea>
		</section>
		<section class="form-group">		
			<label for="">A :</label>
			<input class="form-control" type="email" name="mail"/>
		</section>
		<input class="btn btn-default" type="submit" value="Envoyer"/>
	</form>		
</section>

<?php include('comon/footer.php'); ?>